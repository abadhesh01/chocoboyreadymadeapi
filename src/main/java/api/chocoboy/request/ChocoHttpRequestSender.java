
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 * About Code:
 * -----------
 * This is a ready made code to fetch any kind of HTTP GET request and to get
 * the response status and return the status code along with its respective message.
 */

package api.chocoboy.request;

import api.chocoboy.logger.ChocoLogger;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.HttpURLConnection;
import java.net.URL;

public class ChocoHttpRequestSender {

    // Fields
    private final String httpRequestURL; // This field holds the http request URL.
    private final ChocoLogger chocoLogger; // This field holds the "api.chocoboy.logger.ChocoLogger" class instance.
    private final Logger logger; // This field holds the "org.apache.log4j.Logger" class instance.

    /*
    * Method Name: "ignoreCertificateVerificationForHttpsRequest"
    * Method Parameter(s): No parameter(s) required.
    * Return Type: void
    * Method Job: Bypasses the security certificate verification for the provided request URL.
    */
    public void ignoreCertificateVerificationForHttpsRequest() {

        // Credit: https://stackoverflow.com/questions/33084855/way-to-ignore-ssl-certificate-using-httpsurlconnection

        // Create a trust manager that does not validate certificate chains.
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        // Install the all-trusting trust manager.
        try {
            SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        } catch (Exception exception) {
            logger.error("[Exception => " + exception.getClass().getName() + "] : " + exception.getMessage());
        }
    }

    /*
     * Method Name: "sendRequest"
     * Method Parameter(s): No parameter(s) required.
     * Return Type: api.chocoboy.request.ChocoHttpResponse
     * Method job: Sending the provided HTTP URL as a request and getting the response
     *             and returning the response with respective response message.
     */
    @SuppressWarnings("all")
    public ChocoHttpResponse sendRequest() {

        ChocoHttpResponse response;

        try {
            // If the provided request URL is "null" or "empty" or "blank",
            // throwing an exception and terminating the process.
            if (httpRequestURL == null || httpRequestURL.isEmpty() || httpRequestURL.isBlank()) {
                throw new Exception("Request URL cannot be \"empty\" or \"blank\" or \"null\".");
            }
            // Binding the url to "java.net.URL" class instance.
            URL url = new URL(httpRequestURL);
            // Creating a connection to the url by using "java.net.HttpURLConnection" class instance.
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            // Setting up the type of the connection request.
            connection.setRequestMethod("GET");
            // Setting up the connection properties.
            connection.setRequestProperty("User-Agent", "Mozilla/5.0");
            connection.setRequestProperty("cache-control", "no-cache");
            // Getting the response as a response status code.
            int statusCode = connection.getResponseCode();
            // Binding the response to "api.chocoboy.request.ChocoHttpResponse" class instance.
            response = new ChocoHttpResponse(this.httpRequestURL, statusCode);
        } catch (Exception exception) {
            // Catching exception and terminating the process if any exception occurs
            // and setting up the response status to default value i.e. 0(ZERO).
            //logger.error("[Exception => " + exception.getClass().getName() + "] : " + exception.getMessage());
            response = new ChocoHttpResponse(this.httpRequestURL,
                    "[Exception => " + exception.getClass().getName() + "] : " + exception.getMessage());
            // Printing the response object.
            logger.error(response);
            // Returning the response object.
            return response;
        } catch (Error error) {
            // Catching error and terminating the process if any error occurs.
            // and setting up the response status to default value i.e. 0(ZERO).
            //logger.error("[Error => " + error.getClass().getName() + "] : " + error.getMessage());
            response = new ChocoHttpResponse(this.httpRequestURL,
                    "[Error => " + error.getClass().getName() + "] : " + error.getMessage());
            // Printing the response object.
            logger.error(response);
            // Returning the response object.
            return response;
        }

        // Printing the response object.
        logger.info(response);
        // Returning the response object.
        return  response;
    }

    /*
     * Method Name: "setConsoleOutputProperties"
     * Method Parameter(s): [1] outputPattern(type=String): Defines the output pattern of the logger.
     *                      [2] level(type=org.apache.log4j.Level): Sets the level of the logger.
     * Return Type: void
     * Method job: Configures/Reconfigures the logger for console output only.
     */
    public void setConsoleOutputProperties(String outputPattern, Level level) {
        this.chocoLogger.setConsoleOutputProperties(outputPattern, level);
    }

    /*
     * Method Name: "setConsoleAndFileOutputProperties"
     * Method Parameter(s): [1] outputPattern(type=String): Defines the output pattern of the logger.
     *                      [2] level(type=org.apache.log4j.Level): Sets the level of the logger.
     *                      [3] fileName(type=String): Defines the name of the file where the logger output will be stored.
     * Return Type: void
     * Method job:  Configures/Reconfigures the logger for both console and file output.
     */
    public void setConsoleAndFileOutputProperties(String outputPattern, Level level, String fileName) {
        this.chocoLogger.setConsoleAndFileOutputProperties(outputPattern, level, fileName);
    }

    /*
        Method Name: getLogger
        Method Parameter(s): No parameter(s) required.
        Return Type: org.apache.log4j.Logger
        Method Job: Return the instance of class type "org.apache.log4j.Logger".
    */
    @SuppressWarnings("unused")
    public Logger getLogger() {
        return logger;
    }

    /*
      Constructor 1:
      --------------
      Input 1: httpRequestURL(type=String)
      Input 2: verifySecurityCertificate(type=boolean)
    */
    public ChocoHttpRequestSender(String httpRequestURL, boolean verifySecurityCertificate) {
        // Setting up the "httpRequestURL".
        this.httpRequestURL = httpRequestURL;
        // If "verifySecurityCertificate" is "true", verifying the security certificate.
        if(verifySecurityCertificate) {
            ignoreCertificateVerificationForHttpsRequest();
        }
        // Configuring the logger for console output only.
        this.chocoLogger = new ChocoLogger(ChocoHttpRequestSender.class, Level.INFO);
        logger = this.chocoLogger.getLogger();
    }

    /*
      Constructor 2:
      --------------
      Input 1: httpRequestURL(type=String)
      Input 2: verifySecurityCertificate(type=boolean)
      Input 3: fileName(type=String)
    */
    public ChocoHttpRequestSender(String httpRequestURL, boolean verifySecurityCertificate, String fileName) {
        // Setting up the "httpRequestURL".
        this.httpRequestURL = httpRequestURL;
        // If "verifySecurityCertificate" is "true", verifying the security certificate.
        if(verifySecurityCertificate) {
            ignoreCertificateVerificationForHttpsRequest();
        }
        // Configuring the logger for both console and file output.
        this.chocoLogger = new ChocoLogger(ChocoHttpRequestSender.class, Level.INFO, fileName);
        logger = this.chocoLogger.getLogger();
    }
}
