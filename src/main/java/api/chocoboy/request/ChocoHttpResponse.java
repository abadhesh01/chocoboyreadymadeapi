
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
   About Code:
   -----------
   "api.chocoboy.ChocoHttpResponse" class instance binds the http status code to its
    respective message.
 */

package api.chocoboy.request;

import java.util.Map;
import java.util.TreeMap;

public class ChocoHttpResponse {

    // Fields
    private final String httpRequestURL; // Holds the HTTP request URL.
    private final int status; // Holds http request status code.
    private final String message; // Holds respective message for provided status code.
    private static final Map<Integer, String> RESPONSE_STATUS; // Holds http request status code and its respective message in a key and value pair.

    // Initializing response status codes with respective messages.
    static {

        // For more about HTTP response status,
        // visit: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status#successful_responses

        RESPONSE_STATUS = new TreeMap<>();

        // Default Request
        //RESPONSE_STATUS.put(0, "Program Error.[Date & Time]:" + LocalDateTime.now());

        // Information Responses
        RESPONSE_STATUS.put(100, "Continue");
        RESPONSE_STATUS.put(101,"Switching Protocols");
        RESPONSE_STATUS.put(102,"Processing");
        RESPONSE_STATUS.put(103,"Early Hints");

        // Successful Responses
        RESPONSE_STATUS.put(200,"OK");
        RESPONSE_STATUS.put(201,"Created");
        RESPONSE_STATUS.put(202,"Accepted");
        RESPONSE_STATUS.put(203,"Non-Authoritative Information");
        RESPONSE_STATUS.put(204,"No Content");
        RESPONSE_STATUS.put(205,"Reset Content");
        RESPONSE_STATUS.put(206,"Partial Content");
        RESPONSE_STATUS.put(207,"Multi-Status");
        RESPONSE_STATUS.put(208,"Already Reported");
        RESPONSE_STATUS.put(226,"IM Used");

        // Redirection Messages
        RESPONSE_STATUS.put(300,"Multiple Choices");
        RESPONSE_STATUS.put(301,"Moved Permanently");
        RESPONSE_STATUS.put(302,"Found");
        RESPONSE_STATUS.put(303,"See Other");
        RESPONSE_STATUS.put(304,"Not Modified");
        RESPONSE_STATUS.put(305,"Use Proxy");
        RESPONSE_STATUS.put(306,"Unused");
        RESPONSE_STATUS.put(307,"Temporary Redirect");
        RESPONSE_STATUS.put(308,"Permanent Redirect");

        // Client Error Responses
        RESPONSE_STATUS.put(400,"Bad request");
        RESPONSE_STATUS.put(401,"Unauthorized");
        RESPONSE_STATUS.put(402,"Payment Required");
        RESPONSE_STATUS.put(403,"Forbidden");
        RESPONSE_STATUS.put(404,"Not Found");
        RESPONSE_STATUS.put(405,"Method Not Allowed");
        RESPONSE_STATUS.put(406,"Not Acceptable");
        RESPONSE_STATUS.put(407,"Proxy Authentication Required");
        RESPONSE_STATUS.put(408,"Request Timeout");
        RESPONSE_STATUS.put(409,"Conflict");
        RESPONSE_STATUS.put(410,"Gone");
        RESPONSE_STATUS.put(411,"Length Required");
        RESPONSE_STATUS.put(412,"Precondition Failed");
        RESPONSE_STATUS.put(413,"Payload Too Large");
        RESPONSE_STATUS.put(414,"URI Too Long");
        RESPONSE_STATUS.put(415,"Unsupported Media Type");
        RESPONSE_STATUS.put(416,"Range Not Satisfiable");
        RESPONSE_STATUS.put(417,"Expectation Failed");
        RESPONSE_STATUS.put(418,"I'm a teapot");
        RESPONSE_STATUS.put(421,"Misdirected request");
        RESPONSE_STATUS.put(422,"Un-processable Content");
        RESPONSE_STATUS.put(423,"Locked");
        RESPONSE_STATUS.put(424,"Failed Dependency");
        RESPONSE_STATUS.put(425,"Too Early");
        RESPONSE_STATUS.put(426,"Upgrade Required");
        RESPONSE_STATUS.put(428,"Prediction Required");
        RESPONSE_STATUS.put(429,"Too Many Requests");
        RESPONSE_STATUS.put(431,"Request Header Fields Too Large");
        RESPONSE_STATUS.put(451,"Unavailable For Legal Reason");

        // Server Error Responses
        RESPONSE_STATUS.put(500,"Internal Server Error");
        RESPONSE_STATUS.put(501,"Not Implemented");
        RESPONSE_STATUS.put(502,"Bad Gateway");
        RESPONSE_STATUS.put(503,"Service Unavailable");
        RESPONSE_STATUS.put(504,"Gateway Timeout");
        RESPONSE_STATUS.put(505,"HTTP Version Not Supported");
        RESPONSE_STATUS.put(506,"Variant Also Negotiates");
        RESPONSE_STATUS.put(507,"Insufficient Storage");
        RESPONSE_STATUS.put(508,"Loop Detected");
        RESPONSE_STATUS.put(510,"Not Extended");
        RESPONSE_STATUS.put(511,"Network Authentication Required");
    }

    // Parameterized Constructor 1
    public ChocoHttpResponse(String httpRequestURL, int status) {
        this.httpRequestURL = httpRequestURL;
        this.status = status;
        this.message = RESPONSE_STATUS.get(status);
    }

    // Parameterized Constructor 2
    public ChocoHttpResponse(String httpRequestURL, String customMessage) {
        this.httpRequestURL = httpRequestURL;
        this.status = 0;
        this.message = customMessage;
    }

    // Getters
    @SuppressWarnings("unused")
    public int getStatus() {
        return status;
    }

    @SuppressWarnings("unused")
    public String getMessage() {
        return message;
    }

    // To String.
    @Override
    public String toString() {
        return "ChocoHttpResponse{" +
                "httpRequestURL='" + httpRequestURL + '\'' +
                ", status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}

