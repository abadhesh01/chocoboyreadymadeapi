
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 */

package api.chocoboy.test.testtools;

import api.chocoboy.logger.ChocoLogger;
import api.chocoboy.tools.ChocoTools;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main2 {

    private static final ChocoLogger CHOCO_LOGGER = new ChocoLogger(Main.class, Level.INFO, "GeneratedRandomStrings.txt");
    public static void main(String[] args) {

        Logger logger = CHOCO_LOGGER.getLogger();

        // Generating random strings of size between -3 to 20.
        for (int size = -3; size <= 20; size ++) {
            // Testing "generateRandomString" method of "api.chocoboy.tools.ChocoTools" class.
            String randomString = ChocoTools.generateRandomString(size);
            logger.info("Random String [length='" + randomString.length() + "']:   " + randomString);
        }
        logger.info("----------------------------------------------------");
    }
}
