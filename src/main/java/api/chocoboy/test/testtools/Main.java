
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 */

package api.chocoboy.test.testtools;

import api.chocoboy.logger.ChocoLogger;
import api.chocoboy.tools.ChocoTools;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main {

    private static final ChocoLogger CHOCO_LOGGER = new ChocoLogger(Main.class, Level.INFO, "GeneratedOTP");
    public static void main(String[] args) {

        Logger logger = CHOCO_LOGGER.getLogger();

        // Generating OTPs of length between -3 to 7.
        for(int otpLength = -3; otpLength <= 7; otpLength ++) {
            // Testing "generateOTP" method of "api.chocoboy.tools.ChocoTools" class.
            logger.info(" \t OTP[size='" + otpLength + "']:   " + ChocoTools.generateOTP(otpLength));
        }
        // Testing "generateFourDigitOTP()" method of "api.chocoboy.tools.ChocoTools" class.
        logger.info(" \t Sample 4 digit OTP:   " + ChocoTools.generateFourDigitOTP());
        // Testing "generateSixDigitOTP()" method of "api.chocoboy.tools.ChocoTools" class.
        logger.info(" \t Sample 6 digit OTP:   " + ChocoTools.generateSixDigitOTP());
        logger.info("-----------------------------------------------------------");
    }
}
