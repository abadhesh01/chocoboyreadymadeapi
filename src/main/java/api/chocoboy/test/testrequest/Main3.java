
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 */

package api.chocoboy.test.testrequest;

import api.chocoboy.request.ChocoHttpRequestSender;

public class Main3 {
    public static void main(String[] args) {

        // Testing with console output only.

        // Checking for different http URL requests.

        ChocoHttpRequestSender sender = new ChocoHttpRequestSender(
                "https://www.javatpoint.com/spring-tutorialzz",
                true);
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://www.javapoint.com/spring-tutorial",
                false);
        sender.ignoreCertificateVerificationForHttpsRequest();
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://www.facebook.com/",
                true);
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://www.gitlab.com/abadhesh01",
                true);
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://web.whatsapp.com",
                true);
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(null, true); // Null URL
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("", true); // Empty URL
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("          ", true); // Blank URL
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("www.youtube.com/", true); // URL with no protocols.
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("https://www.youtube.com/", true);
        sender.sendRequest();
    }
}
