
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 */

package api.chocoboy.test.testrequest;

import api.chocoboy.request.ChocoHttpRequestSender;

public class Main4 {
    public static void main(String[] args) {

        // Testing with both console and file outputs.

        // Checking for different http URL requests.

        ChocoHttpRequestSender sender = new ChocoHttpRequestSender(
                "https://www.javatpoint.com/spring-tutorialzz",
                true, "Request_Record");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://www.javapoint.com/spring-tutorial",
                false, "Request_Record");
        sender.ignoreCertificateVerificationForHttpsRequest();
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://www.facebook.com/",
                true, "Request_Record");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://www.gitlab.com/abadhesh01",
                true, "Request_Record");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://web.whatsapp.com",
                true, "Request_Record");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(null, true, // Null URL
                "Request_Record");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("", true, // Empty URL
                "Request_Record");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("          ", true, // Blank URL
                "Request_Record");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("www.youtube.com/", true, // URL with no protocols.
                "Request_Record");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("https://www.youtube.com/", true,
                "Request_Record");
        sender.sendRequest();
    }
}
