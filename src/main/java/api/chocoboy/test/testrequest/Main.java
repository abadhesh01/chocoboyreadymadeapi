
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 */

package api.chocoboy.test.testrequest;

import api.chocoboy.request.ChocoHttpRequestSender;

public class Main {
    public static void main(String[] args) {

        // Testing with console output only.

        // Testing for a correct http request url with security certificate
        // verification disabled.
        ChocoHttpRequestSender sender = new ChocoHttpRequestSender(
                "https://www.javatpoint.com/spring-tutorial",
                false);
        sender.sendRequest(); // Sending request and getting response.

        // Testing for a correct http request url with security certificate
        // verification enabled after constructor call.
        sender = new ChocoHttpRequestSender(
                "https://www.javatpoint.com/spring-tutorial",
                false);
        // Enabling security certificate verification.
        sender.ignoreCertificateVerificationForHttpsRequest();
        sender.sendRequest(); // Sending request and getting response.

        // Testing for a correct http request url with security certificate
        // verification enabled during constructor call.
        sender = new ChocoHttpRequestSender(
                "https://www.javatpoint.com/spring-tutorial",
                true);
        sender.sendRequest(); // Sending request and getting response.
    }
}
