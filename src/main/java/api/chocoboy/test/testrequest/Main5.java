
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 */

package api.chocoboy.test.testrequest;

import api.chocoboy.request.ChocoHttpRequestSender;
import org.apache.log4j.Level;

public class Main5 {
    public static void main(String[] args) {

        // Testing with both console and file output with custom pattern only.
        String customPattern = "%nCustom Pattern:: [%p--(%t)]--{%d{dd/MM/yy}T%d{HH:mm:ss.SSS}}====>%m";

        // Checking for different http URL requests.

        ChocoHttpRequestSender sender = new ChocoHttpRequestSender(
                "https://www.javatpoint.com/spring-tutorialzz",
                true);
        // Switching from console output only to both console and file output.
        sender.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "Request_Record_2");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://www.javapoint.com/spring-tutorial",
                false);
        // Switching from console output only to both console and file output.
        sender.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "Request_Record_2");
        sender.ignoreCertificateVerificationForHttpsRequest();
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://www.facebook.com/",
                true);
        // Switching from console output only to both console and file output.
        sender.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "Request_Record_2");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://www.gitlab.com/abadhesh01",
                true);
        // Switching from console output only to both console and file output.
        sender.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "Request_Record_2");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(
                "https://web.whatsapp.com",
                true);
        // Switching from console output only to both console and file output.
        sender.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "Request_Record_2");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender(null, true); // Null URL
        // Switching from console output only to both console and file output.
        sender.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "Request_Record_2");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("", true); // Empty URL
        // Switching from console output only to both console and file output.
        sender.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "Request_Record_2");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("          ", true); // Blank URL
        // Switching from console output only to both console and file output.
        sender.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "Request_Record_2");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("www.youtube.com/", true); // URL with no protocols.
        // Switching from console output only to both console and file output.
        sender.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "Request_Record_2");
        sender.sendRequest();

        sender = new ChocoHttpRequestSender("https://www.youtube.com/", true);
        // Switching from console output only to both console and file output.
        sender.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "Request_Record_2");
        sender.sendRequest();
    }
}
