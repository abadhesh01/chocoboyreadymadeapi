
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 */

package api.chocoboy.test.testlogger;

import api.chocoboy.logger.ChocoLogger;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main3 {
    public static void main(String[] args) {

        // Testing for switching configuration from console output to both console and file output and vice versa.
        // Setting up configuration for console output only.....
        ChocoLogger chocoLogger = new ChocoLogger(null, null);
        Logger logger = chocoLogger.getLogger();
        logger.setLevel(Level.INFO); // Using Log4j setLevel() method.
        Main.sampleTest(logger);

        String customPattern = "%nCustom Pattern:: [%p--(%t)]--{%d{dd/MM/yy}T%d{HH:mm:ss.SSS}}====>%m";

        // Setting up configuration for both console and file output.....
        chocoLogger.setConsoleAndFileOutputProperties(customPattern, Level.ALL, "main3");
        logger.setLevel(Level.INFO); // Using Log4j setLevel() method.
        Main.sampleTest(logger);

        // Setting up configuration for console output only again.....
        chocoLogger.setConsoleOutputProperties(customPattern, Level.ALL);
        logger.setLevel(Level.INFO); // Using Log4j setLevel() method.
        Main.sampleTest(logger);
    }
}
