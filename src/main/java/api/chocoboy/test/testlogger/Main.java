
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 */

package api.chocoboy.test.testlogger;

import api.chocoboy.logger.ChocoLogger;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main {

    public static void sampleTest(Logger logger) {

        // This method takes a logger object as input parameter and
        // prints some sample messages either on the console or in the
        // file or both.

        logger.info("\n");
        logger.info("OS Name: " + System.getProperty("os.name"));
        logger.info("OS Architecture: " + System.getProperty("os.arch"));
        logger.info("User Name: " + System.getProperty("user.name"));
        logger.info("Java Vendor: " + System.getProperty("java.vendor"));
        logger.info("Java Version: " + System.getProperty("java.version"));
        logger.info("Current Working Directory: " + System.getProperty("user.dir"));
        logger.trace("Sample Trace Message");
        logger.warn("Sample Warning Message");
        logger.error("Sample Error Message");
        logger.fatal("Sample Fatal Error Message");
        logger.debug("Sample Debug Message");
        logger.info("Execution successful." + "\n");
        logger.info("\n");

    }

    public static void main(String[] args) {

        // Testing for "api.chocoboy.logger.ChocoLogger" class constructor 1 with all parameters.
        ChocoLogger chocoLogger = new ChocoLogger(Main.class, Level.ALL);
        Logger logger = chocoLogger.getLogger();
        sampleTest(logger);

        // Testing for "api.chocoboy.logger.ChocoLogger" class constructor 1 with all parameters with "null" values.
        chocoLogger = new ChocoLogger(null, null);
        logger = chocoLogger.getLogger();
        sampleTest(logger);

        // Testing "setConsoleOutputProperties" method of "api.chocoboy.logger.ChocoLogger" class with 1st parameter as "null".
        chocoLogger.setConsoleOutputProperties(null, Level.INFO);
        sampleTest(logger);

        // Testing "setConsoleOutputProperties" method of "api.chocoboy.logger.ChocoLogger" class with 2nd parameter as "null".
        String customPattern = "%nCustom Pattern:: [%p--(%t)]--{%d{dd/MM/yy}T%d{HH:mm:ss.SSS}}====>%m";
        chocoLogger.setConsoleOutputProperties(customPattern, null);
        sampleTest(logger);

        // Testing "setConsoleOutputProperties" method of "api.chocoboy.logger.ChocoLogger" class with all parameters.
        chocoLogger.setConsoleOutputProperties(customPattern, Level.INFO);
        sampleTest(logger);
    }
}
