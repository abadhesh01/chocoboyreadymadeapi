
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 */

package api.chocoboy.test.testlogger;

import api.chocoboy.logger.ChocoLogger;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main2 {
    public static void main(String[] args) {

        // Testing for "api.chocoboy.logger.ChocoLogger" class constructor 2 with all parameters.
        ChocoLogger chocoLogger = new ChocoLogger(Main.class, Level.ALL, "main2_A.txt");
        Logger logger = chocoLogger.getLogger();
        Main.sampleTest(logger);

        // Testing for "api.chocoboy.logger.ChocoLogger" class constructor 2 with all parameters with "null" values.
        chocoLogger = new ChocoLogger(null, null, null);
        logger = chocoLogger.getLogger();
        Main.sampleTest(logger);

        // Testing "setConsoleAndFileOutputProperties" method of "api.chocoboy.logger.ChocoLogger" class with all parameters.
        String customPattern = "%nCustom Pattern:: [%p--(%t)]--{%d{dd/MM/yy}T%d{HH:mm:ss.SSS}}====>%m";
        chocoLogger.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "main2_B.txt");
        Main.sampleTest(logger);

        // Testing "setConsoleAndFileOutputProperties" method of "api.chocoboy.logger.ChocoLogger" class with all parameters with "null" values.
        chocoLogger.setConsoleAndFileOutputProperties(null, null, null);
        Main.sampleTest(logger);
    }
}
