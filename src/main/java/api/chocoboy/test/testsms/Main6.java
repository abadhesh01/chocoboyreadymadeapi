
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 */

package api.chocoboy.test.testsms;

import api.chocoboy.sms.ChocoSMSbyFast2SMS;
import api.chocoboy.tools.ChocoTools;
import org.apache.log4j.Logger;

public class Main6 {
    public static void main(String[] args) {

        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation(); // Deleting the API Key location.

        // Creating instance of class type "api.chocoboy.sms.ChocoSMSbyFast2SMS" with valid field values.
        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS("" + ChocoTools.generateOTP(10),
                "" + "0123456789",
                "p", false);
        Logger logger = sms.getLogger(); // Getting the logger.
        sms.setApiKey(); // Creating the file with location for API Key.
        TestChocoSMSbyFast2SMS.insertTextIntoAPIKeyFile(); // Inserting dummy text to API Key file.
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the instance of class type "api.chocoboy.sms.ChocoSMSbyFast2SMS" for testing.
        TestChocoSMSbyFast2SMS.testSMS(); // Sending the text message.
        TestChocoSMSbyFast2SMS.testVoice(); // Sending the voice message.
        logger.info("----------------------------------------------------------------------------------------------------------------------------------------------"
                   + "----------------------------------------------------------------------------------------------------------------------------------------------"
                   + "----------------------------------------------------------------------------------------------------------------------------------------------");

        sms = sms.getClone(false); // Cloning and setting up the instance of class type "api.chocoboy.sms.ChocoSMSbyFast2SMS" for testing.
        logger = sms.getLogger(); // Getting the logger.
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the instance of class type "api.chocoboy.sms.ChocoSMSbyFast2SMS" for testing.
        TestChocoSMSbyFast2SMS.testSMS(); // Sending the text message.
        TestChocoSMSbyFast2SMS.testSMS(); // Sending the text message again.
        TestChocoSMSbyFast2SMS.testVoice(); // Sending the voice message.
        logger.info("----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------"
                   + "----------------------------------------------------------------------------------------------------------------------------------------------");

        sms = sms.getClone(true); // Cloning and setting up the instance of class type "api.chocoboy.sms.ChocoSMSbyFast2SMS" for testing.
        logger = sms.getLogger(); // Getting the logger.
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the instance of class type "api.chocoboy.sms.ChocoSMSbyFast2SMS" for testing.
        TestChocoSMSbyFast2SMS.testSMS(); // Sending the text message.
        TestChocoSMSbyFast2SMS.testSMS(); // Sending the text message again.
        TestChocoSMSbyFast2SMS.testVoice(); // Sending the voice message.
        logger.info("----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------");

        sms = sms.getClone(false); // Cloning and setting up the instance of class type "api.chocoboy.sms.ChocoSMSbyFast2SMS" for testing.
        sms.alterSecurityCertificateVerificationMode(); // Altering the security certificate verification mode.
        logger = sms.getLogger(); // Getting the logger.
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the instance of class type "api.chocoboy.sms.ChocoSMSbyFast2SMS" for testing.
        TestChocoSMSbyFast2SMS.testVoice(); // Sending the voice message.
        TestChocoSMSbyFast2SMS.testSMS(); // Sending the text message.
        TestChocoSMSbyFast2SMS.testVoice(); // Sending the text message again.
        logger.info("----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------");
    }
}
