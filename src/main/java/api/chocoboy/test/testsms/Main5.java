
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 *
 *  Note: Testing the logger for the methods "setConsoleOutputProperties" and
 *        "setConsoleAndFileOutputProperties" for the class "api.chocoboy.sms.ChocoSMSbyFast2SMS".
 */

package api.chocoboy.test.testsms;

import api.chocoboy.sms.ChocoSMSbyFast2SMS;
import api.chocoboy.tools.ChocoTools;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main5 {
    public static void main(String[] args) {

        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS(
                "" + ChocoTools.generateRandomString(6) + "#" + ChocoTools.generateSixDigitOTP(),
                "0123456789", "p", false, "MsgTest2"); // File output will be produced.

        Logger logger = sms.getLogger();
        logger.info(ChocoTools.generateRandomString(10));
        logger.info(ChocoTools.generateRandomString(10));

        // Testing with console output only with custom pattern.
        String customPattern = "%nCustom Pattern:: [%p--(%t)]--{%d{dd/MM/yy}T%d{HH:mm:ss.SSS}}====>%m";
        sms.setConsoleOutputProperties(customPattern, Level.INFO);
        logger.info(ChocoTools.generateRandomString(10));
        logger.info(ChocoTools.generateRandomString(10));

        sms.setConsoleOutputProperties(null, null);
        logger.info(ChocoTools.generateRandomString(10));
        logger.info(ChocoTools.generateRandomString(10));

        sms.setConsoleAndFileOutputProperties(null, null, null);
        logger.info(ChocoTools.generateRandomString(10));
        logger.info(ChocoTools.generateRandomString(10));

        sms.setConsoleAndFileOutputProperties(null, null, "");
        logger.info(ChocoTools.generateRandomString(10));
        logger.info(ChocoTools.generateRandomString(10));

        sms.setConsoleAndFileOutputProperties(null, null, "     ");
        logger.info(ChocoTools.generateRandomString(10));
        logger.info(ChocoTools.generateRandomString(10));

        sms.setConsoleAndFileOutputProperties(null, null, "MsgTest2"); // File output will be produced.
        logger.info(ChocoTools.generateRandomString(10));
        logger.info(ChocoTools.generateRandomString(10));

        sms.setConsoleAndFileOutputProperties(customPattern, Level.INFO, "MsgTest2"); // File output will be produced with custom pattern.
        logger.info(ChocoTools.generateRandomString(10));
        logger.info(ChocoTools.generateRandomString(10));

    }
}
