
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 *
 *  Note: [1] Testing the validation of the fields "message", "mobileNumber" and "route" for the instance of
 *            "api.chocoboy.sms.ChocoSMSbyFast2SMS" class for sending Voice SMS.
 *        [2] Also validating the methods:
 *            verifyCriteria,
 *            send &
 *            sendVoiceSMS
 *            for sending Voice SMS.
 */

package api.chocoboy.test.testsms;

import api.chocoboy.sms.ChocoSMSbyFast2SMS;
import api.chocoboy.tools.ChocoTools;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main3 {

    /*
     * Method Name: testForFieldsMessageAndMobileNumberForVoiceSMS
     * Method Parameter(s): [1] message(type=String)
     *                      [2] mobileNumber(type=String)
     * Return Type: void
     * Method Job: Tests the validation of the fields "message" and "mobileNumber" for sending Voice SMS.
     */
    public static void testForFieldsMessageAndMobileNumberForVoiceSMS(String message, String mobileNumber) {
        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file if exists.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation(); // Deleting the API Key location if exists.

        // Creating a "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance using its 1st parameterized constructor.
        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS(message, mobileNumber, "", true);
        // Getting the logger object.
        Logger logger = sms.getLogger();
        // Setting up the logger level to ALL for tracing purpose only.
        logger.setLevel(Level.ALL);

        logger.info(sms.setApiKey()); // Creating the API Key file.
        TestChocoSMSbyFast2SMS.insertTextIntoAPIKeyFile(); // Inserting some dummy text to the file containing API Key.
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance.
        TestChocoSMSbyFast2SMS.testVoice(); // Testing a sample Voice SMS.
        logger.info("\"message\": \"" + message + "\""); // Printing the provided message
        logger.info("\"mobileNumber\": \"" + mobileNumber + "\""); // Printing the provided mobile number.
        logger.info("----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------");
    }

    /*
     * Method Name: testForFieldRouteForVoiceSMS
     * Method Parameter(s): route(type=String)
     * Return Type: void
     * Method Job: Tests the validation of the field "route" for sending Voice SMS.
     */
    public static void testForFieldRouteForVoiceSMS(String route) {
        String message = ChocoTools.generateOTP(10); // Generating a sample 10 digit SMS.
        String mobileNumber = "0123456789"; // Sample 10 digit mobile number.

        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file if exists.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation(); // Deleting the API Key location if exists.

        // Creating a "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance using its 1st parameterized constructor.
        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS(message, mobileNumber, route, true);
        // Getting the logger object.
        Logger logger = sms.getLogger();
        // Setting up the logger level to ALL for tracing purpose only.
        logger.setLevel(Level.ALL);

        logger.info(sms.setApiKey()); // Creating the API Key file.
        TestChocoSMSbyFast2SMS.insertTextIntoAPIKeyFile(); // Inserting some dummy text to the file containing API Key.
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance.
        TestChocoSMSbyFast2SMS.testVoice(); // Testing a sample Voice SMS.

        logger.info("----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------");
    }

    public static void main(String[] args) {

        String message = ChocoTools.generateOTP(10); // Generating a 10 digit number for Voice SMS.
        String mobileNumber = "0123456789"; // Sample 10 digit mobile number.

        testForFieldsMessageAndMobileNumberForVoiceSMS(null, mobileNumber); // Testing for field "message" with "null" value.
        testForFieldsMessageAndMobileNumberForVoiceSMS("", mobileNumber); // Testing for field "message" with empty value.
        testForFieldsMessageAndMobileNumberForVoiceSMS("     ", mobileNumber); // Testing for field "message" with blank value.
        testForFieldsMessageAndMobileNumberForVoiceSMS("" + ChocoTools.generateOTP(11) , mobileNumber); // Testing for field "message" with more than 10 characters.
        testForFieldsMessageAndMobileNumberForVoiceSMS("AB" + ChocoTools.generateOTP(8) , mobileNumber); // Testing for field "message" with non-digit characters.

        testForFieldsMessageAndMobileNumberForVoiceSMS(message, null); // Testing for field "mobileNumber" with "null" value.
        testForFieldsMessageAndMobileNumberForVoiceSMS(message, ""); // Testing for field "mobileNumber" with empty value.
        testForFieldsMessageAndMobileNumberForVoiceSMS(message, "     "); // Testing for field "mobileNumber" with blank value.
        testForFieldsMessageAndMobileNumberForVoiceSMS(message, "+91" + ChocoTools.generateOTP(7)); // Testing for field "mobileNumber" with a non-digit character.
        testForFieldsMessageAndMobileNumberForVoiceSMS(message, "" + ChocoTools.generateOTP(9)); // Testing for field "mobileNumber" with less than 10 digits.
        testForFieldsMessageAndMobileNumberForVoiceSMS(message, "" + ChocoTools.generateOTP(11)); // Testing for field "mobileNumber" with more than 10 digits.

        testForFieldRouteForVoiceSMS("p"); // Testing for the field "route" with value "p".
        testForFieldRouteForVoiceSMS("q"); // Testing for the field "route" with value "q".
        testForFieldRouteForVoiceSMS("t"); // Testing for the field "route" with value "t".
        testForFieldRouteForVoiceSMS("otp"); // Testing for the field "route" with value "otp".
        testForFieldRouteForVoiceSMS(""); // Testing for the field "route" with empty value.
        testForFieldRouteForVoiceSMS("     "); // Testing for the field "route" with blank value.
        testForFieldRouteForVoiceSMS(null); // Testing for the field "route" with value "null".

        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation(); // Deleting the API Key location.
        // Creating a "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance using its 1st parameterized constructor.
        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS(message, mobileNumber, "", true);
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance.
        TestChocoSMSbyFast2SMS.testVoice(); // Testing a sample Voice SMS.
        sms.getLogger()
        .info("----------------------------------------------------------------------------------------------------------------------------------------------"
                + "----------------------------------------------------------------------------------------------------------------------------------------------"
                + "----------------------------------------------------------------------------------------------------------------------------------------------");

        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation(); // Deleting the API Key location.
        TestChocoSMSbyFast2SMS.insertTextIntoAPIKeyFile(); // Inserting some text into API Key file.
        // Creating a "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance using its 1st parameterized constructor.
        sms = new ChocoSMSbyFast2SMS(message, mobileNumber, "", true);
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance.
        TestChocoSMSbyFast2SMS.testVoice(); // Testing a sample Voice SMS.
        sms.getLogger()
                .info("----------------------------------------------------------------------------------------------------------------------------------------------"
                        + "----------------------------------------------------------------------------------------------------------------------------------------------"
                        + "----------------------------------------------------------------------------------------------------------------------------------------------");
        
    }
}
