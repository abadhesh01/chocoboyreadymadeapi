
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 *
 *  Note: Testing for the method "setApiKey".
 */

package api.chocoboy.test.testsms;

import api.chocoboy.request.ChocoHttpResponse;
import api.chocoboy.sms.ChocoSMSbyFast2SMS;
import org.apache.log4j.Logger;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {

        // Creating a scanner object to take keyboard input.
        Scanner scanner = new Scanner(System.in);

        // Creating the "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance to test it.
        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS();
        // Getting the logger for console output.
        Logger logger = sms.getLogger();

        // Deleting the API Key file if exists.
        TestChocoSMSbyFast2SMS.deleteApiKeyFile();
        // Deleting the location of the API Key file if exists.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation();

        // Either setting up or creating the file for API Key.
        logger.trace("Press \"Enter/Return\" key to set API Key for Fast2SMS services.....");
        logger.trace(scanner.nextLine());
        ChocoHttpResponse response = sms.setApiKey(); // Setting up the API Key.
        logger.info(response);

        // Deleting the file containing the API Key.
        logger.trace("Press \"Enter/Return\" key to delete the file containing the API Key.....");
        logger.trace(scanner.nextLine());
        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file if exists.

        // Either setting up or creating file for API Key.
        logger.trace("Press \"Enter/Return\" key to set API Key for Fast2SMS services.....");
        logger.trace(scanner.nextLine());
        response = sms.setApiKey(); // Setting up the API Key.
        logger.info(response);

        // Deleting the location containing the API Key.
        logger.trace("Press \"Enter/Return\" key to delete the location containing the API Key.....");
        logger.trace(scanner.nextLine());
        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file if exists.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation(); // Deleting the API Key location if exists.

        // Either setting up or creating the file for API Key.
        logger.trace("Press \"Enter/Return\" key to set API Key for Fast2SMS services.....");
        logger.trace(scanner.nextLine());
        response = sms.setApiKey(); // Setting up the API Key.
        logger.info(response);

        // Either setting up or creating the file for API Key again.
        logger.trace("Press \"Enter/Return\" key to set API Key for Fast2SMS services again.....");
        logger.trace(scanner.nextLine());
        response = sms.setApiKey(); // Setting up the API Key.
        logger.info(response);

        // Inserting some text into the file containing the API Key
        logger.trace("Press \"Enter/Return\" key to insert some text in the file for API Key.....");
        logger.trace(scanner.nextLine());
        TestChocoSMSbyFast2SMS.insertTextIntoAPIKeyFile(); // Inserting some texts into the API Key file.
        response = sms.setApiKey(); // Setting up the API Key.
        logger.info(response);

        logger.trace("Press \"Enter/Return\" key to send a sample SMS.....");
        logger.trace(scanner.nextLine());
        response = sms.sendSMS(); // Sending a sample SMS.
        logger.info(response);

        logger.trace("Press \"Enter/Return\" key to send a sample Voice SMS.....");
        logger.trace(scanner.nextLine());
        response = sms.sendVoiceSMS(); // Sending a sample Voice SMS.
        logger.info(response);

        // Closing the scanner to prevent resource leak.
        scanner.close();
    }
}
