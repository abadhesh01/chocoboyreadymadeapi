
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 *
 *  Note: [1] Testing the validation of the fields "message", "mobileNumber" and "route" for the instance of
 *            "api.chocoboy.sms.ChocoSMSbyFast2SMS" class for sending text SMS.
 *        [2] Also validating the methods:
 *            configureRoute,
 *            verifyCriteria,
 *            send &
 *            sendSMS
 *            for sending text SMS.
 */

package api.chocoboy.test.testsms;

import api.chocoboy.sms.ChocoSMSbyFast2SMS;
import api.chocoboy.tools.ChocoTools;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main2 {

    /*
     * Method Name: testForFieldsMessageAndMobileNumberForSMS
     * Method Parameter(s): [1] message(type=String)
     *                      [2] mobileNumber(type=String)
     * Return Type: void
     * Method Job: Tests the validation of the fields "message" and "mobileNumber" for sending SMS.
     */
    public static void testForFieldsMessageAndMobileNumberForSMS(String message, String mobileNumber) {
        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file if exists.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation(); // Deleting the API Key location if exists.

        // Creating a "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance using its 1st parameterized constructor.
        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS(message, mobileNumber, "", true);
        // Getting the logger object.
        Logger logger = sms.getLogger();
        // Setting up the logger level to ALL for tracing purpose only.
        logger.setLevel(Level.ALL);

        logger.info(sms.setApiKey()); // Creating the API Key file.
        TestChocoSMSbyFast2SMS.insertTextIntoAPIKeyFile(); // Inserting some dummy text to the file containing API Key.
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance.
        TestChocoSMSbyFast2SMS.testSMS(); // Testing a sample SMS.
        logger.info("\"message\": \"" + message + "\""); // Printing the provided message
        logger.info("\"mobileNumber\": \"" + mobileNumber + "\""); // Printing the provided mobile number.
        logger.info("----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------");
    }

    /*
     * Method Name: testForFieldRouteForSMS
     * Method Parameter(s): route(type=String)
     * Return Type: void
     * Method Job: Tests the validation of the field "route" for sending SMS.
     */
    public static void testForFieldRouteForSMS(String route) {
        String message = ChocoTools.generateRandomString(4) + "#" + ChocoTools.generateOTP(4); // Generating a sample SMS.
        String mobileNumber = "0123456789"; // Sample 10 digit mobile number.

        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file if exists.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation(); // Deleting the API Key location if exists.

        // Creating a "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance using its 1st parameterized constructor.
        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS(message, mobileNumber, route, true);
        // Getting the logger object.
        Logger logger = sms.getLogger();
        // Setting up the logger level to ALL for tracing purpose only.
        logger.setLevel(Level.ALL);

        logger.info(sms.setApiKey()); // Creating the API Key file.
        TestChocoSMSbyFast2SMS.insertTextIntoAPIKeyFile(); // Inserting some dummy text to the file containing API Key.
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance.
        TestChocoSMSbyFast2SMS.testSMS(); // Testing a sample SMS.

        logger.info("----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------"
                  + "----------------------------------------------------------------------------------------------------------------------------------------------");
    }

    public static void main(String[] args) {

        String message = ChocoTools.generateRandomString(4) + "#" + ChocoTools.generateOTP(4); // Creating a sample SMS.
        String mobileNumber = "0123456789"; // Sample 10 digit mobile number.

        testForFieldsMessageAndMobileNumberForSMS(null, mobileNumber); // Testing for field "message" with "null" value.
        testForFieldsMessageAndMobileNumberForSMS("", mobileNumber); // Testing for field "message" with empty value.
        testForFieldsMessageAndMobileNumberForSMS("     ", mobileNumber); // Testing for field "message" with blank value.

        testForFieldsMessageAndMobileNumberForSMS(message, null); // Testing for field "mobileNumber" with "null" value.
        testForFieldsMessageAndMobileNumberForSMS(message, ""); // Testing for field "mobileNumber" with empty value.
        testForFieldsMessageAndMobileNumberForSMS(message, "     "); // Testing for field "mobileNumber" with blank value.
        testForFieldsMessageAndMobileNumberForSMS(message, "+91" + ChocoTools.generateOTP(7)); // Testing for field "mobileNumber" with a non-digit character.
        testForFieldsMessageAndMobileNumberForSMS(message, "" + ChocoTools.generateOTP(9)); // Testing for field "mobileNumber" with less than 10 digits.
        testForFieldsMessageAndMobileNumberForSMS(message, "" + ChocoTools.generateOTP(11)); // Testing for field "mobileNumber" with more than 10 digits.

        testForFieldRouteForSMS("p"); // Testing for the field "route" with value "p".
        testForFieldRouteForSMS("q"); // Testing for the field "route" with value "q".
        testForFieldRouteForSMS("t"); // Testing for the field "route" with value "t".
        testForFieldRouteForSMS("otp"); // Testing for the field "route" with value "otp".
        testForFieldRouteForSMS(""); // Testing for the field "route" with empty value.
        testForFieldRouteForSMS("     "); // Testing for the field "route" with blank value.
        testForFieldRouteForSMS(null); // Testing for the field "route" with value "null".

        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation(); // Deleting the API Key location.
        // Creating a "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance using its 1st parameterized constructor.
        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS(message, mobileNumber, "", true);
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance.
        TestChocoSMSbyFast2SMS.testSMS(); // Testing a sample SMS.
        sms.getLogger()
        .info("----------------------------------------------------------------------------------------------------------------------------------------------"
                + "----------------------------------------------------------------------------------------------------------------------------------------------"
                + "----------------------------------------------------------------------------------------------------------------------------------------------");

        TestChocoSMSbyFast2SMS.deleteApiKeyFile(); // Deleting the API Key file.
        TestChocoSMSbyFast2SMS.deleteApiKeyLocation(); // Deleting the API Key location.
        TestChocoSMSbyFast2SMS.insertTextIntoAPIKeyFile(); // Inserting some text into API Key file.
        // Creating a "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance using its 1st parameterized constructor.
        sms = new ChocoSMSbyFast2SMS(message, mobileNumber, "", true);
        TestChocoSMSbyFast2SMS.setSms(sms); // Setting up the "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance.
        TestChocoSMSbyFast2SMS.testSMS(); // Testing a sample SMS.
        sms.getLogger()
                .info("----------------------------------------------------------------------------------------------------------------------------------------------"
                        + "----------------------------------------------------------------------------------------------------------------------------------------------"
                        + "----------------------------------------------------------------------------------------------------------------------------------------------");

    }
}
