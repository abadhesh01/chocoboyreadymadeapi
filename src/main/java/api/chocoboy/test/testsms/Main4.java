
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 *
 *  Note: Testing the logger for the method "getClone" and 2nd constructor of the class "api.chocoboy.sms.ChocoSMSbyFast2SMS".
 */

package api.chocoboy.test.testsms;

import api.chocoboy.sms.ChocoSMSbyFast2SMS;
import api.chocoboy.tools.ChocoTools;
import org.apache.log4j.Logger;


public class Main4 {
    public static void main(String[] args) {

        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS(
                "" + ChocoTools.generateRandomString(6) + "#" + ChocoTools.generateSixDigitOTP(),
                "0123456789", "p", true);
        Logger logger = sms.getLogger();
        logger.info(sms);

        sms = sms.getClone(true);
        logger = sms.getLogger();
        logger.info(sms);

        sms = sms.getClone(true, null);
        logger = sms.getLogger();
        logger.info(sms);

        sms = sms.getClone(true, "");
        logger = sms.getLogger();
        logger.info(sms);

        sms = sms.getClone(true, "     ");
        logger = sms.getLogger();
        logger.info(sms);

        sms = sms.getClone(true, "MsgTest"); // File output will be produced.
        logger = sms.getLogger();
        logger.info(sms);

        sms = new ChocoSMSbyFast2SMS(
                "" + ChocoTools.generateRandomString(6) + "#" + ChocoTools.generateSixDigitOTP(),
                "0123456789", "p", true, null);
        logger = sms.getLogger();
        logger.info(sms);

        sms = new ChocoSMSbyFast2SMS(
                "" + ChocoTools.generateRandomString(6) + "#" + ChocoTools.generateSixDigitOTP(),
                "0123456789", "p", true, "");
        logger = sms.getLogger();
        logger.info(sms);

        sms = new ChocoSMSbyFast2SMS(
                "" + ChocoTools.generateRandomString(6) + "#" + ChocoTools.generateSixDigitOTP(),
                "0123456789", "p", true, "     ");
        logger = sms.getLogger();
        logger.info(sms);

        sms = new ChocoSMSbyFast2SMS(
                "" + ChocoTools.generateRandomString(6) + "#" + ChocoTools.generateSixDigitOTP(),
                "0123456789", "p", true, "MsgTest"); // File output will be produced.
        logger = sms.getLogger();
        logger.info(sms);

    }
}
