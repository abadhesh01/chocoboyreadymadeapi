
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 *  About Code: This code was for manual testing purpose but, was not removed after testing.
 *              This can be used for checking for the api is working or not in the later times.
 *
 *  Note: This class contains some methods in order to reduce the efforts in testing for the class
 *        "api.chocoboy.sms.ChocoSMSbyFast2SMS".
 */

package api.chocoboy.test.testsms;


import api.chocoboy.request.ChocoHttpResponse;
import api.chocoboy.sms.ChocoSMSbyFast2SMS;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.*;

@SuppressWarnings("unused")
public class TestChocoSMSbyFast2SMS {

    // Fields
    // Holds the "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance over which,
    // testing is going to be performed.
    private static ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS();
    // Location details for the file containing the API Key.
    private static final String apiKeyLocation = "C:\\Users\\" + System.getProperty("user.name") + "\\Documents\\ChocoSMSbyFast2SMS\\";
    // Path to the file containing the API Key.
    private static final String apiKeyFile = "C:\\Users\\" + System.getProperty("user.name") + "\\Documents\\ChocoSMSbyFast2SMS\\API_KEY.txt";

    // Setter to set up the field "sms".
    public static void setSms(ChocoSMSbyFast2SMS sms) {
        TestChocoSMSbyFast2SMS.sms = sms;
    }

    /*
     *  Method Name: deleteApiKeyFile
     *  Return Type: void
     *  Method Job: Deletes the file containing the API Key i.e.
     *              "C:\\Users\<Your username>\Documents\ChocoSMSbyFast2SMS\API_KEY.txt"
     */
    public static void deleteApiKeyFile() {
        Logger logger = sms.getLogger();
        logger.setLevel(Level.ALL);
        boolean apiKeyFileExist = new File(TestChocoSMSbyFast2SMS.apiKeyFile).exists();
        logger.info("API Key File Exists: " + apiKeyFileExist);
        if(apiKeyFileExist) {
            logger.info("Deleting API Key File.......");
            boolean deleted = new File(TestChocoSMSbyFast2SMS.apiKeyFile).delete();
            logger.info("File Deleted: " + deleted);
        }
    }

    /*
     *  Method Name: deleteApiKeyLocation
     *  Return Type: void
     *  Method Job: Deletes the location containing the API Key i.e.
     *              "C:\\Users\<Your username>\Documents\ChocoSMSbyFast2SMS\"
     */
    public static void deleteApiKeyLocation() {
        Logger logger = sms.getLogger();
        logger.setLevel(Level.ALL);
        boolean apiKeyLocationExist = new File(apiKeyLocation).exists();
        logger.info("API Key Location Exists: " + apiKeyLocationExist);
        if(apiKeyLocationExist) {
            logger.info("Deleting API Key Location.......");
            boolean deleted = new File(apiKeyLocation).delete();
            logger.info("Location Deleted: " + deleted);
        }
    }

    /*
     *  Method Name: insertTextIntoAPIKeyFile
     *  Return Type: void
     *  Method Job: Inserts some text into the file containing the API Key i.e.
     *               "C:\\Users\<Your username>\Documents\ChocoSMSbyFast2SMS\API_KEY.txt"
     */
    public static void insertTextIntoAPIKeyFile() {
        sms.setApiKey(); // Setting up the API Key.
        Logger logger = sms.getLogger();
        logger.setLevel(Level.ALL);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(apiKeyFile);
            String sampleText = "\n\n API KEY ";
            StringBuilder textData = new StringBuilder();
            for (int count = 0; count <= 10 ; count ++) {
                textData.append(sampleText).append(" ").append(count).append("\n\n");
            }
            byte[] byteData = textData.toString().getBytes();
            fileOutputStream.write(byteData);
            fileOutputStream.close();
        } catch (IOException exception) {
            logger.error("Text insertion to the file \"" + apiKeyFile + "\" failed.");
            logger.error(exception + " : " + exception.getMessage());
            return;
        }
        logger.info("Text inserted to the file \"" + apiKeyFile + "\" successfully.");
    }


    /*
     * Method Name: testSMS
     * Return Type: void
     * Method: Tests by sending a sample SMS.
     */
    public static void testSMS()  {
        Logger logger = sms.getLogger();
        logger.setLevel(Level.ALL);

        ChocoHttpResponse response = sms.setApiKey(); // Setting up the API Key.
        logger.info(response);

        response = sms.sendSMS(); // Sending the SMS.
        logger.info(response);
    }

    /*
     * Method Name: testVoice
     * Return Type: void
     * Method: Tests by sending a sample Voice SMS.
     */
    public static void testVoice()  {
        Logger logger = sms.getLogger();
        logger.setLevel(Level.ALL);

        ChocoHttpResponse response = sms.setApiKey(); // Setting up the API Key.
        logger.info(response);

        response = sms.sendVoiceSMS(); // Sending the Voice SMS.
        logger.info(response);
    }

}
