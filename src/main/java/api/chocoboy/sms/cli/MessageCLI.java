
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 * About Code:
 * -----------
 * [1] A sample command line interface based application
 *     to take necessary user inputs(mobileNumber, route)
 *     from keyboard and send a sample otp based text message.
 *
 * [2] This code internally uses "send" method of "api.chocoboy.sms.cli.MessageSendingMachine"
 *     class to send text message.
 */

package api.chocoboy.sms.cli;

import api.chocoboy.logger.ChocoLogger;
import api.chocoboy.tools.ChocoTools;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class MessageCLI {
    public static void main(String[] args) {

        // Creating a scanner object to take keyboard inputs.
        Scanner keyboardInput = new Scanner(System.in);

        // Creating logger object to print output on console.
        ChocoLogger chocoLogger = new ChocoLogger(MessageCLI.class, Level.INFO);
        Logger logger = chocoLogger.getLogger();

        // Taking keyboard inputs.
        logger.info("\n\nText Message Sender App\n***********************\n");
        String message = "Your OTP for verification: " + ChocoTools.generateSixDigitOTP() + ".";
        logger.info("Message to be sent: \"" + message + "\"");
        logger.info("Enter your mobile number: ");
        String mobileNumber = keyboardInput.nextLine(); // Input 1: Mobile Number
        logger.info("Choose the route through which the message will be send:");
        logger.info("Enter 'p' for promotional message service.");
        logger.info("Enter 'q' for quick message service.");
        logger.info("Enter 't' for transactional message service.");
        logger.info("Enter 'otp' for OTP message service.");
        logger.info("Enter your choice:");
        String route = keyboardInput.nextLine(); // Input 2: Route
        route = route.toLowerCase();

        // Sending the text message.
        MessageSendingMachine.send(message, mobileNumber, route, false);

        // Closing the scanner object to prevent resource leak.
        keyboardInput.close();

    }
}
