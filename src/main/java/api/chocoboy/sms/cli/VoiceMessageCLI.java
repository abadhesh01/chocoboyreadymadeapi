
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 * About Code:
 * -----------
 * [1] A sample command line interface based application
 *     to take necessary user input(mobileNumber)
 *     from keyboard and send the voice message.
 *
 * [2] This code internally uses "send" method of "api.chocoboy.sms.cli.MessageSendingMachine"
 *     class to send voice message.
 *
 * [3] CAUTION: For voice messages only, numeric characters (0-9) are allowed up to 10 digits.
 */

package api.chocoboy.sms.cli;

import api.chocoboy.logger.ChocoLogger;
import api.chocoboy.tools.ChocoTools;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class VoiceMessageCLI {
    public static void main(String[] args) {

        // Creating a scanner object to take keyboard inputs.
        Scanner keyboardInput = new Scanner(System.in);

        // Creating logger object to print output on console.
        ChocoLogger chocoLogger = new ChocoLogger(VoiceMessageCLI.class, Level.INFO);
        Logger logger = chocoLogger.getLogger();

        // Taking keyboard inputs
        logger.info("\n\nVoice Message Sender App\n************************\n");
        logger.info("CAUTION: For voice messages only, numeric characters" +
                    " (0-9) are allowed up to 10 digits.");
        String message = ChocoTools.generateOTP(10);
        logger.info("OTP to be sent: \"" + message + "\"");
        logger.info("Enter your mobile number: ");
        String mobileNumber = keyboardInput.nextLine(); // Input: Mobile Number

        // Sending the voice message.
        MessageSendingMachine.send(message, mobileNumber, null , true);

        // Closing the scanner object to prevent resource leak.
        keyboardInput.close();

    }
}
