
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 * About Code:
 * -----------
 * The class "api.chocoboy.sms.cli.MessageSendingMachine" contains a method "send" which
 * internally uses "api.chocoboy.sms.ChocoSMSbyFast2SMS" class instance and its
 * functions to send either a text message or a voice message.
 */

package api.chocoboy.sms.cli;

import api.chocoboy.request.ChocoHttpResponse;
import api.chocoboy.sms.ChocoSMSbyFast2SMS;
import org.apache.log4j.Logger;

public class MessageSendingMachine {

    /*
     * Method Name: send
     * Method Parameter(s): [1] message(type=String): Holds the message to be sent.
     *                      [2] mobileNumber(type=String): Holds the mobile number of the receiver of the message.
     *                      [3] route(type=String): Holds the route through which the message will be sent.
     *                      [4] isVoice(type=boolean): Defines whether the message is a text message or a voice message.
     * Return Type: void
     * Method Job: Sends either a text message or a voice message.
     */
    public static void send(String message, String mobileNumber, String route, boolean isVoice) {

        // Creating the instance of "api.chocoboy.sms.ChocoSMSbyFast2SMS"
        // using its 2nd parameterized constructor.
        ChocoSMSbyFast2SMS sms = new ChocoSMSbyFast2SMS(
                message,
                mobileNumber,
                route,
                true,
                "MSG_LOGS.txt");

        // Getting the logger object to print the output on both console and file.
        Logger logger = sms.getLogger();

        // Printing the information provided by the user.
        logger.info("\n\n");
        logger.info("\"Message\": \"" + sms.getMessage() + "\"");
        logger.info("\"Mobile Number\": \"" + sms.getMobileNumber() + "\"");
        if (isVoice) {
            logger.info("\"Message Type\": \"Voice\"");
        } else {
            logger.info("\"Message Type\": \"Text\"");
            logger.info("\"Route\": \"" + sms.getRoute() + "\"");
        }

        // Setting up the API Key.
        ChocoHttpResponse response = sms.setApiKey();

        // Printing the response after
        // setting up the API Key.
        logger.info("API Key Status: " + response.getMessage());

        // If the API Key has not been set successfully,
        // returning a negative response.
        if (!response.getMessage().equals("OK")) {
            return;
        }

        // If the message type is voice, then
        // sending the message as a voice call
        // and getting the response.
        if (isVoice) {
            response = sms.sendVoiceSMS();
        }
        // Otherwise, sending the message as a
        // text message and returning the response.
        else {
            response = sms.sendSMS();
        }

        // Printing the response.
        logger.info(response + "\n");
    }

}
