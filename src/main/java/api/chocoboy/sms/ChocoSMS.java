
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 * About Code:
 * -----------
 * The interface "api.chocoboy.sms.ChocoSMS" contains an abstract method sendSMS()
 * which can be implemented to send SMS to mobile numbers.
 */

package api.chocoboy.sms;

/*
 * Method Name: send
 * Method Parameter(s): No parameter(s) required.
 * Return Type: java.lang.Object
 * Method Job: Sends SMS to a mobile number.
 */
@FunctionalInterface
public interface ChocoSMS {
    Object sendSMS();
}
