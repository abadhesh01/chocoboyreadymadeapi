
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 * About Code:
 * -----------
 * [1] The class "api.chocoboy.sms.ChocoSMSbyFast2SMS" implements the interfaces
 *     "api.chocoboy.sms.ChocoSMS" and "api.chocoboy.sms.ChocoVoice" which are used to
 *     send SMS and Voice SMS to mobile numbers respectively.
 *
 * [2] The class "api.chocoboy.sms.ChocoSMSbyFast2SMS" internally uses Fast2SMS API
 *     to send SMS or OTP based Voice SMS to any mobile number.
 *
 * [3] For more about Fast2SMS API Services, visit: https://www.fast2sms.com
 *
 * [4] Note: The SMS services are available for Indian(+91) numbers only as per my knowledge.
 */

package api.chocoboy.sms;

import api.chocoboy.logger.ChocoLogger;
import api.chocoboy.request.ChocoHttpRequestSender;
import api.chocoboy.request.ChocoHttpResponse;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

@SuppressWarnings({"unused"})
public class ChocoSMSbyFast2SMS implements ChocoSMS, ChocoVoice {

    // Fields
    private final String message; // Holds the message to be sent and cannot be modified.
    private final String mobileNumber; // Holds the mobile number of the receiver of the message and cannot be modified.
    private final String route; // Holds the route through which the message will be sent and cannot be modified.
    private boolean used; // Determines whether the message current object has been used or not.
    private boolean enableSecurityCertificateVerification; // Determines whether the security certificate verification will be done or not.
    private String fileName; // File where the output is to be stored.
    private final ChocoLogger chocoLogger; // Holds "api.chocoboy.logger.ChocoLogger" class instance which conatins the logger("org.apache.log4j.Logger")
                                           // configurartion and cannot be modified.
    private final Logger logger; // Holds the logger object which will print the output either on console or both on console and file and cannot be modified.
    private String apiKey; // Holds the apiKey to send the SMS.
    private String outputPattern; // Defines the output pattern for either console output or both console and file output.

    // To String.
    @Override
    public String toString() {
        return "ChocoSMSbyFast2SMS{" +
                "message='" + message + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", route='" + route + '\'' +
                ", used=" + used +
                '}';
    }

    // Getters
    public String getMessage() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        return this.message;
    }

    public String getMobileNumber() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        return this.mobileNumber;
    }

    public String getRoute() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        return this.route;
    }

    public boolean isUsed() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        return this.used;
    }

    public boolean isEnableSecurityCertificateVerification() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        return this.enableSecurityCertificateVerification;
    }

    public String getFileName() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        return this.fileName;
    }

    /*
     * Method Name: getLogger
     * Method Parameter(s): No parameter(s) required.
     * Return Type: org.apache.log4j.Logger
     * Method Job: Returns the instance of class type "org.apache.log4j.Logger".
     */
    public Logger getLogger() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        return this.logger;
    }

    /*
     * Method Name: getClone
     * Method Parameter(s): enableSecurityCertificateVerification(type=boolean) -> Determines whether the security certificate verification will be done or not.
     * Return Type: api.chocoboy.sms.ChocoSMSbyFast2SMS
     * Method: Returns the clone of the current instance.
     */
    public ChocoSMSbyFast2SMS getClone(boolean enableSecurityCertificateVerification) {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        // Returning the clone.
        return new ChocoSMSbyFast2SMS(this.message, this.mobileNumber, this.route, enableSecurityCertificateVerification);
    }

    /*
     * Method Name: getClone
     * Method Parameter(s): [1] enableSecurityCertificateVerification(type=boolean) -> Determines whether the security certificate verification will be done or not.
     *                      [2] fileName(type=String) -> Name of the file where the logger output will be saved.
     * Return Type: api.chocoboy.sms.ChocoSMSbyFast2SMS
     * Method: Returns the clone of the current instance.
     */
    public ChocoSMSbyFast2SMS getClone(boolean enableSecurityCertificateVerification, String fileName) {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        // If the parameter "fileName" is "null" or empty or blank, then calling the "getClone" method
        // with single parameter and returning its result.
        if (fileName == null || fileName.isEmpty() || fileName.isBlank()) {
            return getClone(enableSecurityCertificateVerification);
        }

        // Returning the clone.
        return new ChocoSMSbyFast2SMS(this.message, this.mobileNumber, this.route, enableSecurityCertificateVerification, fileName);
    }

    /*
     * Method Name: alterSecurityCertificateVerificationMode
     * Method Parameter(s): No parameter(s) required.
     * Return Type: void
     * Method Job: Alters the boolean field "enableSecurityCertificateVerification".
     */
    public void alterSecurityCertificateVerificationMode() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        // Altering the boolean field "enableSecurityCertificateVerification".
        this.enableSecurityCertificateVerification = !this.enableSecurityCertificateVerification;
    }

    /*
     * Method Name: setApiKey
     * Method Parameter(s): No parameter(s) required.
     * Return Type: api.chocoboy.request.ChocoHttpResponse
     * Method Job: Sets the API key to enable messaging services.
     */
    public ChocoHttpResponse setApiKey() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        String emptyURL = "N/A";
        String apiKeyLocation = "C:\\Users\\" + System.getProperty("user.name")
                + "\\Documents\\ChocoSMSbyFast2SMS\\";
        String apiKeyFileName = "API_KEY.txt";

        // If the location containing the API Key file does not exist,
        // then creating the API Key file location.
        boolean locationExists = !(new File(apiKeyLocation).mkdir());

        // If the file containing the API Key does not exist,
        // then creating the file for API Key.
        boolean apiKeyFileExists;
        try {
            apiKeyFileExists = !(new File(apiKeyLocation + apiKeyFileName).createNewFile());
        } catch (IOException exception) {
             // Response for if the creation of the file for the API Key fails.
             return new ChocoHttpResponse(emptyURL,
                     "[!]: Could not create the file \"" + apiKeyLocation + apiKeyFileName + "\".");
        }

        // Response for if either the location or the file for the API Key does not exist.
        if (!locationExists || !apiKeyFileExists) {
            return new ChocoHttpResponse(emptyURL,
                    "[!]: Save your API key at \"" + apiKeyLocation + apiKeyFileName + "\".");
        }

        // Reading the file containing the API key and getting the API Key and storing it into the field "apiKey".
        Scanner fileReader;
        try {
            fileReader = new Scanner(new File(apiKeyLocation + apiKeyFileName));
        } catch (FileNotFoundException e) {
            // Response for if the file for the API Key could not be read.
            return new ChocoHttpResponse(emptyURL,
                    "[!]: Could not read the file \"" + apiKeyLocation + apiKeyFileName + "\".");
        }
        this.apiKey = "";
        StringBuilder apiKey = new StringBuilder();
        while (fileReader.hasNextLine()) {
           apiKey.append(fileReader.nextLine());
        }
        this.apiKey += apiKey.toString();

        // Closing the field "fileReader" to prevent resource leak.
        fileReader.close();

        // Response for if the field "apiKey" is "null" or empty or blank.
        if (this.apiKey == null || this.apiKey.isEmpty() || this.apiKey.isBlank()) {
            return new ChocoHttpResponse(emptyURL,
                    "[!]: \"null\" or Empty or Blank API Key is not allowed! Please set your API key at \""
                            + apiKeyLocation + apiKeyFileName + "\".");
        }

        logger.trace("API Key: \"" + this.apiKey + "\"");  // Tracing the API Key.
        // Response for if the API Key has been set successfully.
        // return new ChocoHttpResponse(emptyURL, "[:)] API Key has been set successfully.");
        return new ChocoHttpResponse(emptyURL, "OK");
    }

    /*
     * Method Name: processOutputPattern
     * Method Parameter(s): outputPattern(type=String) -> Output pattern format for logger.
     * Return Type: String
     * Method Job: Returns the processed output pattern.
     */
    private String processOutputPattern(String outputPattern) {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        if // the field "outputPattern" is "null" or empty or blank then, returning the default output pattern.
        (outputPattern == null || outputPattern.isEmpty() || outputPattern.isBlank()) {
            return this.chocoLogger.outputPattern;
        }

        // Else returning the provided output pattern.
        return  outputPattern;
    }

    /*
     * Method Name: setConsoleOutputProperties
     * Method Parameter(s): [1] outputPattern(type=String): Defines the output pattern of the logger.
     *                      [2] level(type=org.apache.log4j.Level): Sets the level of the logger.
     * Return Type: void
     * Method Job: Configures/Reconfigures the logger for console output only.
     */
    public void setConsoleOutputProperties(String outputPattern, Level level) {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        // Setting up the field "fileName" to "null" as no file output is required.
        this.fileName = null;

        // Setting up the output pattern to the field "outputPattern".
        this.outputPattern = processOutputPattern(outputPattern);

        // Configuring the logger for the provided output pattern and logger level.
        this.chocoLogger.setConsoleOutputProperties(this.outputPattern, level);
    }

    /*
     * Method Name: setConsoleAndFileOutputProperties
     * Method Parameter(s): [1] outputPattern(type=String): Defines the output pattern of the logger.
     *                      [2] level(type=org.apache.log4j.Level): Sets the level of the logger.
     *                      [3] fileName(type=String): Defines the name of the file where the logger output will be stored.
     * Return Type: void
     * Method Job:  Configures/Reconfigures the logger for both console and file output.
     * CAUTION: File Output will work only in Windows 10 and above.
     *          Updates for file output in other OSs will be provided as soon as possible.
     */
    public void setConsoleAndFileOutputProperties(String outputPattern, Level level, String fileName) {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        // If the parameter "fileName" is "null" or empty or blank, then calling
        // the method "setConsoleOutputProperties" and "return".
        if (fileName == null || fileName.isEmpty() || fileName.isBlank()) {
            this.chocoLogger.setConsoleOutputProperties(outputPattern, level);
            return;
        }

        // Setting the file name to the field "fileName".
        this.fileName = fileName;

        // Setting up the output pattern to the field "outputPattern".
        this.outputPattern = processOutputPattern(outputPattern);

        // Configuring the logger for provided output pattern, logger level and file name.
        this.chocoLogger.setConsoleAndFileOutputProperties(this.outputPattern, level, fileName);
    }

    /*
     * Method Name: configureRoute
     * Method Parameter(s): route(type=String) -> Takes the route as input.
     * Return Type: String
     * Method Job: Checks and returns the valid route.
     */
    private String configureRoute(String route) {

        // Verifying the route.
        boolean verifyRoute = (route != null) &&
                (route.equals("p") || // "p" -> Promotional SMS
                        route.equals("q") || // "q" -> Quick SMS
                        route.equals("t") || // "t" -> Transactional SMS
                        route.equals("otp")); // "otp" -> OTP SMS

        // If the route is valid, returning the route.
        if(verifyRoute) {
            return route;
        }

        // If the route is invalid, returning the route for promotional SMS
        // as default route.
        return "p"; // "p" -> Promotional SMS
    }

    /*
     * Method Name: verifyCriteria
     * Method Parameter(s): No parameter(s) required.
     * Return Type: api.chocoboy.request.ChocoHttpResponse
     * Method Job: Validates the fields "used", "message", "mobileNumber" and "apiKey"
     *             and returns a response with a proper message.
     */
    private ChocoHttpResponse verifyCriteria() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        String emptyURL = "N/A";

        // If the current instance is already used, then returning a negative response with a proper message.
        if (this.used) {
            return new ChocoHttpResponse(emptyURL, "[!]: The " + this.getClass().getName()
                    + "@" + this.hashCode() + " has already been used.");
        }

        // If the field "message" is "null" or "empty" or "blank", then returning a negative response with a proper message.
        boolean verifyMessage = (this.message == null) ||
                (this.message.isEmpty()) ||
                (this.message.isBlank());
        if (verifyMessage) {
            return new ChocoHttpResponse(emptyURL, "[!]: The field 'message' cannot be 'null' or 'empty' or 'blank'.");
        }

        // If the field "mobileNumber" is "null" or "empty" or "blank", then returning a negative response with a proper message.
        boolean verifyMobileNumber = (this.mobileNumber == null) ||
                (this.mobileNumber.isEmpty()) ||
                (this.mobileNumber.isBlank());
        if (verifyMobileNumber) {
            return new ChocoHttpResponse(emptyURL, "[!]: The field 'mobileNumber' cannot be 'null' or 'empty' or 'blank'.");
        }

        // Make sure the field "mobileNumber" contains digits only.
        // Or else return a negative response with a proper message.
        char[] mobileNumberCharacterArray = this.mobileNumber.toCharArray();
        for (char characterAtIndex : mobileNumberCharacterArray) {
            if (!(characterAtIndex >= '0' && characterAtIndex <= '9')) {
                return new ChocoHttpResponse(emptyURL, "[!]: The field \"mobileNumber\" should contain digits only.");
            }
        }

        // Make sure the field "mobileNumber" exactly contains 10 digits.
        // Or else return a negative response with a proper message.
        if (mobileNumber.length() != 10) {
            return new ChocoHttpResponse(emptyURL, "[!]: The field \"mobileNumber\" should exactly contain 10 digits.");
        }

        // If the field "apiKey" is "null" or "empty" or "blank", then returning a negative response with a proper message.
        boolean verifyApiKey = (this.apiKey == null) ||
                (this.apiKey.isEmpty()) ||
                (this.apiKey.isBlank());
        if (verifyApiKey) {
            return new ChocoHttpResponse(emptyURL, "[!]: The field 'apiKey' cannot be 'null' or 'empty' or 'blank'.");
        }

        // If all the criteria passed, then encoding the field "apiKay" and returning a positive response with a proper message.
        this.apiKey = URLEncoder.encode(this.apiKey, StandardCharsets.UTF_8);
        return new ChocoHttpResponse(emptyURL, "OK");
    }

    /*
     * Method Name: send
     * Method Parameter(s): url(type=String) -> Holds the URL to be fetched.
     * Return Type: api.chocoboy.request.ChocoHttpResponse
     * Method Job: Fetches the provided URL and returns either a positive or negative response.
     */
    private ChocoHttpResponse send(String url) {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        // Instantiating and configuring the object of the class type "api.chocoboy.request.ChocoHttpRequestSender" to send the SMS.
        ChocoHttpRequestSender sender;
        if // the field "fileName" is "null" then, creating the instance of type
           // "api.chocoboy.request.ChocoHttpRequestSender" by calling its 1st constructor.
        (this.fileName == null) {
            sender = new ChocoHttpRequestSender(url, this.enableSecurityCertificateVerification);
            // If the custom output pattern is provided, then configure it.
            if (!this.outputPattern.equals(this.chocoLogger.outputPattern)) {
                sender.setConsoleOutputProperties(this.outputPattern, Level.INFO);
            }
        } else // creating the instance of type "api.chocoboy.request.ChocoHttpRequestSender" by calling
               // its 2nd constructor.
        {
            sender = new ChocoHttpRequestSender(url, this.enableSecurityCertificateVerification, this.fileName);
            // If the custom output pattern is provided, then configure it.
            if (!this.outputPattern.equals(this.chocoLogger.outputPattern)) {
                sender.setConsoleAndFileOutputProperties(this.outputPattern, Level.INFO, this.fileName);
            }
        }

        // Now, the current object has been utilized.
        // So, setting up the field "used" as true.
        this.used = true;

        // Sending the SMS and returning the response.
        return sender.sendRequest();
    }

    /*
     * Method Name: sendSMS()
     * Method Parameter(s): No parameters required.
     * Return Type: api.chocoboy.request.ChocoHttpResponse
     * Method Job: Fetches SMS through URL and returns either a positive or negative response.
     */
    @Override
    public ChocoHttpResponse sendSMS() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        // Verifying the fields "used", "message", "mobileNumber" and "apiKey"
        // and checking whether the appropriate data is available to send the SMS.
        ChocoHttpResponse verify = verifyCriteria();
        if (!verify.getMessage().equals("OK")) {
            return verify;
        }

        // Tracing the route.
        logger.trace("Route: " + this.route);

        // Generating the request URL.
        String url;
        url = "https://www.fast2sms.com/dev/bulkV2?authorization=" + this.apiKey
                + "&message=" + URLEncoder.encode(message, StandardCharsets.UTF_8) + "&language=english&route=" + this.route
                + "&numbers=" + mobileNumber;

        // Fetching the URL as a request, getting the response and returning the response.
        return this.send(url);
    }

    /*
     * Method Name: sendVoiceSMS()
     * Method Parameter(s): No parameters required.
     * Return Type: api.chocoboy.request.ChocoHttpResponse
     * Method Job: Fetches Voice SMS through URL and returns either a positive or negative response.
     */
    @Override
    public ChocoHttpResponse sendVoiceSMS() {
        // Tracing the current method in execution.
        logger.trace("Current method in execution: " + new Throwable().getStackTrace()[0].getMethodName());

        // Verifying the fields "used", "message", "mobileNumber" and "apiKey"
        // and checking whether the appropriate data is available to send the Voice SMS.
        ChocoHttpResponse verify = verifyCriteria();
        if (!verify.getMessage().equals("OK")) {
            return verify;
        }

        String emptyURL = "N/A";

        // The message should contain digits only.
        // Or else return a negative response with a proper message.
        char[] messageCharacterArray = this.message.toCharArray();
        for (char characterAtIndex : messageCharacterArray) {
            if (!(characterAtIndex >= '0' && characterAtIndex <= '9')) {
                return new ChocoHttpResponse(emptyURL,
                        "[!]: For Voice SMS only, the field \"message\" should contain digits only.");
            }
        }

        // The length of the message should not be more than 10 characters.
        // Or else return a negative response with a proper message.
        if (message.length() > 10) {
            return new ChocoHttpResponse(emptyURL,
                    "[!]: For Voice SMS only, the field \"message\" should contain maximum 10 characters.");
        }

        // Generating the request URL.
        String url;
        url = "https://www.fast2sms.com/dev/voice?authorization=" + this.apiKey
                + "&variables_values=" + message + "&route=otp"
                + "&numbers=" + mobileNumber;

        // Fetching the URL as a request, getting the response and returning the response.
        return this.send(url);
    }

    // Default Constructor
    public ChocoSMSbyFast2SMS() {
        this.message = null;
        this.mobileNumber = null;
        this.route = null;
        this.used = true;
        this.enableSecurityCertificateVerification = false;
        this.chocoLogger = new ChocoLogger(ChocoSMSbyFast2SMS.class, Level.INFO);
        this.logger = chocoLogger.getLogger();
        this.outputPattern = this.chocoLogger.outputPattern;
    }

    /*
     * Parameterized Constructor 1:
     * ----------------------------
     * Input 1: message(type=String) -> Message to be sent.
     * Input 2: mobileNumber(type=String) -> Receiver mobile number.
     * Input 3: route(type=String) -> Route through which the message will be sent.
     * Input 4: enableSecurityCertificateVerification(type=boolean) -> Determines whether the security certificate verification
     *                                                                 will be done or not.
     */
    public ChocoSMSbyFast2SMS(String message, String mobileNumber, String route, boolean enableSecurityCertificateVerification) {
        this.message = message;
        this.mobileNumber = mobileNumber;
        this.route = configureRoute(route);
        this.enableSecurityCertificateVerification = enableSecurityCertificateVerification;
        this.chocoLogger = new ChocoLogger(ChocoSMSbyFast2SMS.class, Level.INFO);
        this.logger = chocoLogger.getLogger();
        this.outputPattern = this.chocoLogger.outputPattern;
    }

    /*
     * Parameterized Constructor 2:
     * ----------------------------
     * Input 1: message(type=String) -> Message to be sent.
     * Input 2: mobileNumber(type=String) -> Receiver mobile number.
     * Input 3: route(type=String) -> Route through which the message will be sent.
     * Input 4: enableSecurityCertificateVerification(type=boolean) -> Determines whether the security certificate verification
     *                                                                 will be done or not.
     * Input 5: fileName(type=String) -> Name of the file where logger output will be stored.
     * CAUTION: File Output will work only in Windows 10 and above.
     *          Updates for file output in other OSs will be provided as soon as possible.
     */
    public ChocoSMSbyFast2SMS(String message, String mobileNumber, String route, boolean enableSecurityCertificateVerification, String fileName) {
        this.message = message;
        this.mobileNumber = mobileNumber;
        this.route = configureRoute(route);
        this.enableSecurityCertificateVerification = enableSecurityCertificateVerification;
        if (fileName == null || fileName.isEmpty() || fileName.isBlank()) {
            this.chocoLogger = new ChocoLogger(ChocoSMSbyFast2SMS.class, Level.INFO);
        } else {
            this.chocoLogger = new ChocoLogger(ChocoSMSbyFast2SMS.class, Level.INFO, fileName);
            this.fileName = fileName;
        }
        this.logger = chocoLogger.getLogger();
        this.outputPattern = this.chocoLogger.outputPattern;
    }
}
