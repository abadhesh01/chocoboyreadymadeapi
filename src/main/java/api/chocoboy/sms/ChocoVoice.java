
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 * About Code:
 * -----------
 * The interface "api.chocoboy.sms.ChocoVoice" contains an abstract method sendVoice()
 * which can be implemented to send Voice SMS to mobile numbers.
 */

package api.chocoboy.sms;

/*
 * Method Name: send
 * Method Parameter(s): No parameter(s) required.
 * Return Type: java.lang.Object
 * Method Job: Sends Voice SMS to a mobile number.
 */
@FunctionalInterface
public interface ChocoVoice {
    Object sendVoiceSMS();
}
