
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 * About Code:
 * -----------
 * [1] This is a ready made logger api which internally uses Log4j version 1.2.17.
 *
 * [2] This api reduces the number of steps for configurations which makes it easy to use.
 *
 * [3] Anyone can use it easily by just writing one or two lines of codes.
 *
 * [4] As it internally uses Log4j version 1.2.17, you can get the logger object by calling the "getLogger()"
 *     non-static method through "api.chocoboy.logger.ChocoLogger" class instance and use it for writing
 *     different types of messages on the console.
 *
 * [5] You can also configure the logger object by yourself but, not required.
 *
 * [6] For displaying messages with custom pattern, refer:
 *      https://logging.apache.org/log4j/2.x/manual/layouts.html
 *
 * [7] Basic patterns:
 *     %d -> Date and Time
 *     %t -> Method Name
 *     %p -> Message Category
 *     %m -> Message
 *     %n -> New Line
 *
 *     For date and time:
 *     d -> Day
 *     M -> Month
 *     y -> Year
 *     H -> Hour
 *     m -> Minute(s)
 *     s -> Second(s)
 *     S -> Millisecond(s)
 */

package api.chocoboy.logger;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.util.Objects;
import java.util.Properties;

public class ChocoLogger {

    // Attributes
    private final Logger logger; // This non-static final field holds the main logger object.
    private final Properties loggerProperties = new Properties(); // This non-static final field holds the properties for logger configuration.
    public final String outputPattern = "%n[%p(%t){Date:%d{dd MMM yyyy}-&-Time:%d{HH:mm:ss.SSS}}]::%m"; // This non-static final field is the default logger pattern.

    /*
        Method Name: setConsoleOutputProperties
        Method Parameter(s): [1] outputPattern(type=String): Defines the output pattern of the logger.
                             [2] level(type=org.apache.log4j.Level): Sets the level of the logger.
        Return Type: void
        Method Job: Configures/Reconfigures the logger for console output only.
     */
    public void setConsoleOutputProperties(String outputPattern, Level level) {
        // If the logger level is "null", defining the level as "org.apache.log4j.Level.ALL".
        if (level == null) {
            level = Level.ALL;
        }
        LogManager.resetConfiguration(); // Resetting the logger configuration.
        // Setting up the logger properties.
        loggerProperties.setProperty("log4j.rootLogger", level + ", stdout");
        loggerProperties.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
        loggerProperties.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
        // If the provided output pattern for logger output is "null", then setting the output pattern to default pattern otherwise keeping the same pattern.
        loggerProperties.setProperty("log4j.appender.stdout.layout.ConversionPattern", Objects.requireNonNullElse(outputPattern, this.outputPattern));
        // Adding the logger properties to the logger configuration.
        PropertyConfigurator.configure(loggerProperties);
    }


    /*
        Method Name: setConsoleAndFileOutputProperties
        Method Parameter(s): [1] outputPattern(type=String): Defines the output pattern of the logger.
                             [2] level(type=org.apache.log4j.Level): Sets the level of the logger.
                             [3] fileName(type=String): Defines the name of the file where the logger output will be stored.
        Return Type: void
        Method Job: Configures/Reconfigures the logger for both console and file output.
        [Note]: This function will only work in Windows OS. For other OS, updates will be provided as soon as possible.
    */
    public void setConsoleAndFileOutputProperties(String outputPattern, Level level, String fileName) {
        // If the logger level is "null", defining the level as "org.apache.log4j.Level.ALL".
        if (level == null) {
            level = Level.ALL;
        }
        // If the file name is "null" or has no name or has blank name, then naming the file as "Default".
        if (fileName == null || fileName.length() == 0 || fileName.isBlank()) {
            //noinspection ReassignedVariable
            fileName = "Default";
        }
        LogManager.resetConfiguration(); // Resetting the logger configuration.
        // Setting up the logger properties for console output.
        loggerProperties.setProperty("log4j.rootLogger", level + ",stdout, logfile"); // Resetting the logger configuration.
        loggerProperties.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
        loggerProperties.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
        // If the provided output pattern for logger output is "null", then setting the output pattern to default pattern otherwise, keeping the same pattern.
        loggerProperties.setProperty("log4j.appender.stdout.layout.ConversionPattern", Objects.requireNonNullElse(outputPattern, this.outputPattern));
        // Setting up the logger properties for file output.
        /*
           -> Creating a folder named "ChocoLogs" in "C:\Users\<your-username>\Documents" as
              "C:\Users\<your-username>\Documents\ChocoLogs" if not exists to store all the log files
              with logger outputs. This format is for Windows OS only.
        */
        String loggingLocation = "C:\\Users\\" + System.getProperty("user.name") + "\\Documents\\ChocoLogs";
        //noinspection ResultOfMethodCallIgnored
        new File(loggingLocation).mkdir();
        loggerProperties.setProperty("log4j.appender.logfile.File", loggingLocation + "\\" + fileName);
        loggerProperties.setProperty("log4j.appender.logfile", "org.apache.log4j.FileAppender");
        loggerProperties.setProperty("log4j.appender.logfile.layout", "org.apache.log4j.PatternLayout");
        // If the provided output pattern for logger output is "null", then setting the output pattern to default pattern otherwise, keeping the same pattern.
        loggerProperties.setProperty("log4j.appender.logfile.layout.ConversionPattern", Objects.requireNonNullElse(outputPattern, this.outputPattern));
        // Adding the logger properties to the logger configuration.
        PropertyConfigurator.configure(loggerProperties);
    }


    /*
        Method Name: getLogger
        Method Parameter(s): No parameter(s) required.
        Return Type: org.apache.log4j.Logger
        Method Job: Return the instance of class type "org.apache.log4j.Logger".
    */
    public Logger getLogger() {
        return logger;
    }

    /*
       Constructor 1:
       --------------
       Input 1: clazz(type=Class)
       Input 2: level(type=org.apache.log4j.Level)
       Job: Configures the logger for console output.
    */
    public ChocoLogger(@SuppressWarnings("all") Class clazz, Level level) {
        // Instantiating logger with the provided class name.
        logger = Logger.getLogger(Objects.requireNonNullElse(clazz, ChocoLogger.class));
        // Configuring the logger for console output only.
        setConsoleOutputProperties(null, level);
    }

    /*
       Constructor 2:
       --------------
       Input 1: clazz(type=Class)
       Input 2: level(type=org.apache.log4j.Level)
       Input 3: fileName(type=String)
       Job: Configures the logger for both console and file output.
    */
    public ChocoLogger(@SuppressWarnings("all") Class clazz, Level level, String fileName) {
        // Instantiating logger with the provided class name.
        logger = Logger.getLogger(Objects.requireNonNullElse(clazz, ChocoLogger.class));
        // Configuring the logger for both console output and file output.
        setConsoleAndFileOutputProperties(null, level, fileName);
    }
}
