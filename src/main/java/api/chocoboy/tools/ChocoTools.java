
/*
 * Author: Abadhesh Mishra [Employee Id: 8117322]
 * GitLab: https://gitlab.com/abadhesh01
 */

/*
 * About Code:
 * -----------
 * The class "api.chocoboy.tools.ChocoTools" contains some utility methods.
 */

package api.chocoboy.tools;

import java.util.concurrent.ThreadLocalRandom;

public class ChocoTools {

    /*
     * Method Name: generateOTP
     * Method Parameter(s): numberOfDigits(type=int) -> Defines the number of digits in the OTP to be generated.
     * Return Type: String
     * Method Job: Generates an OTP of digits of at least 4.
     */
    public static String generateOTP(int numberOfDigits) {

        // If the "numberOfDigits" is less than 4, setting it to 4.
        if (numberOfDigits < 4) {
            numberOfDigits = 4;
        }

        // Creating a "java.lang.StringBuilder" class instance
        // to append some digits of count "numberOfDigits".
        StringBuilder otp = new StringBuilder();

        // Generating a single digit(0-9) each time and appending it
        // to the "java.lang.StringBuilder" instance for "numberOfDigits"
        // number of times.
        for (;
             numberOfDigits > 0;
             numberOfDigits --) {
             otp.append(ThreadLocalRandom.current().nextInt(0, 10));
        }

        // Converting the "java.lang.StringBuilder" class instance to "java.lang.String"
        // class instance and returning it.
        return otp.toString();
    }

    /*
     * Method Name: generateFourDigitOTP
     * Method Parameters: No parameter(s) requires.
     * Return Type: String
     * Method Job: Generates a 4 digit OTP.
     * Note: It internally uses "generateOTP" method.
     */
    public static String generateFourDigitOTP() {
        return generateOTP(4);
    }

    /*
     * Method Name: generateSixDigitOTP
     * Method Parameter(s): No parameter(s) requires.
     * Return Type: String
     * Method Job: Generates a 6 digit OTP.
     * Note: It internally uses "generateOTP" method.
     */
    public static String generateSixDigitOTP() {
        return  generateOTP(6);
    }

    /*
     * Method Name: generateRandomString
     * Method Parameter(s): size(type=int) -> Defines the length of the random string.
     * Return Type: String
     * Method Job: Generates a random string of length at least 4.
     * Note: It generates a random string that contains no special characters and spaces.
     *       The random string contains characters between [A-Z], [a-z] and [0-9].
     */
    public static String generateRandomString(int size) {

        // If the given size of the random string to be generated
        // is less than 4, then setting the size to 4.
        if(size < 4) {
            size = 4;
        }

        // Creating a "java.lang.StringBuilder" class instance
        // to append some characters of count "size".
        StringBuilder randomString = new StringBuilder();

        // Generating each single character of the random string of about
        // "size" times.
        for(;
             size > 0;
             size --) {

            // Generating "choice" (between 1 and 3 inclusive).
            int choice = ThreadLocalRandom.current().nextInt(1, 4);

            // Initializing "origin" and "bound" to "0(ZERO)".
            int origin = 0;
            int bound = 0;

            // Choice for a character between 'A' to 'Z'.
            // If "choice" is "1", setting the "origin" to 'A' and "bound" to ('Z' + 1).
            if(choice == 1) {
               origin = 'A';
               bound = 'Z' + 1;
            }

            // Choice for a character between 'a' to 'z'.
            // If "choice" is "2", setting the "origin" to 'a' and "bound" to ('z' + 1).
            if(choice == 2) {
                origin = 'a';
                bound = 'z' + 1;
            }

            // Choice for a character between '0' to '9'.
            // If "choice" is "3", setting the "origin" to '0' and "bound" to ('9' + 1).
            if(choice == 3) {
                origin = '0';
                bound = '9' + 1;
            }

            // Generating a random character between "origin" and "bound"
            // and appending it to "java.lang.StringBuilder" class instance.
            randomString.append((char)ThreadLocalRandom.current().nextInt(origin, bound));
        }

        // Converting the "java.lang.StringBuilder" class instance to "java.lang.String"
        // class instance and returning it.
        return randomString.toString();
    }
}
